variable "region" {
  type = string
}

variable "name" {
  type = string
}
variable "aws_elastic_beanstalk_environment" {}
