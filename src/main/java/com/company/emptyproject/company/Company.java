package com.company.emptyproject.company;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "company")
public class Company {
    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private Type type;

    @Column(name = "number")
    private Integer number;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "name")
    private String name;

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}