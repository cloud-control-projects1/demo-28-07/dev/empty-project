import { gql } from "@amplicode/gql";
import { Type } from "@amplicode/gql/graphql";
import { ResultOf } from "@graphql-typed-document-node/core";
import { useCallback } from "react";
import {
  DateInput,
  Edit,
  NumberInput,
  SimpleForm,
  TextInput,
  useNotify,
  useRedirect,
  useUpdate,
} from "react-admin";
import { FieldValues, SubmitHandler } from "react-hook-form";
import { checkServerValidationErrors } from "../../../core/error/checkServerValidationError";
import { MAX_INT_VALUE, MIN_INT_VALUE } from "../../../core/format/constants";
import { formatNumber } from "../../../core/format/formatNumber";
import { parseNumber } from "../../../core/format/parseNumber";
import { EnumInput } from "../../../core/inputs/EnumInput";

const COMPANY = gql(`query Company($id: ID!) {
  company(id: $id) {
    date
    id
    name
    number
    type
  }
}`);
const UPDATE_COMPANY = gql(`mutation UpdateCompany($input: CompanyInput!) {
  updateCompany(input: $input) {
    date
    id
    name
    number
    type
  }
}`);

export const CompanyEdit = () => {
  const queryOptions = {
    meta: {
      query: COMPANY,
      resultDataPath: null,
    },
  };

  const redirect = useRedirect();
  const notify = useNotify();
  const [update] = useUpdate();

  const save: SubmitHandler<FieldValues> = useCallback(
    async (data: FieldValues) => {
      try {
        const params = { data, meta: { mutation: UPDATE_COMPANY } };
        const options = { returnPromise: true };

        await update("Company", params, options);

        notify("ra.notification.updated", { messageArgs: { smart_count: 1 } });
        redirect("list", "Company");
      } catch (response: any) {
        console.log("update failed with error", response);
        return checkServerValidationErrors(response, notify);
      }
    },
    [update, notify, redirect]
  );

  return (
    <Edit<ItemType> mutationMode="pessimistic" queryOptions={queryOptions}>
      <SimpleForm onSubmit={save}>
        <DateInput source="date" name="date" />
        <TextInput source="name" name="name" />
        <NumberInput
          source="number"
          name="number"
          max={MAX_INT_VALUE}
          min={MIN_INT_VALUE}
          format={formatNumber}
          parse={parseNumber}
        />
        <EnumInput name="type" source="type" enumTypeName="Type" enum={Type} />
      </SimpleForm>
    </Edit>
  );
};

/**
 * Type of data object received when executing the query
 */
type QueryResultType = ResultOf<typeof COMPANY>;
/**
 * Type of the item loaded by executing the query
 */
type ItemType = { id: string } & Exclude<QueryResultType["company"], undefined>;
