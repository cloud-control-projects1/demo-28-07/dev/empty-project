import { gql } from "@amplicode/gql";
import { Company, Type } from "@amplicode/gql/graphql";
import { ResultOf } from "@graphql-typed-document-node/core";
import {
  Datagrid,
  DeleteButton,
  EditButton,
  FunctionField,
  List,
  NumberField,
  TextField,
} from "react-admin";
import { EnumField } from "../../../core/fields/EnumField";
import { renderDate } from "../../../core/format/renderDate";

const COMPANY_LIST = gql(`query CompanyList {
  companyList {
    date
    id
    name
    number
    type
  }
}`);

const DELETE_COMPANY = gql(`mutation DeleteCompany($id: ID!) {
  deleteCompany(id: $id) 
}`);

export const CompanyList = () => {
  const queryOptions = {
    meta: {
      query: COMPANY_LIST,
      resultDataPath: "",
    },
  };

  return (
    <List<ItemType> queryOptions={queryOptions} exporter={false} pagination={false}>
      <Datagrid rowClick="show" bulkActionButtons={false}>
        <TextField source="id" sortable={false} />

        <FunctionField source="date" render={(record: Company) => renderDate(record.date)} />
        <TextField source="name" sortable={false} />
        <NumberField source="number" sortable={false} />
        <EnumField source="type" enumTypeName="Type" enum={Type} sortable={false} />

        <EditButton />
        <DeleteButton
          mutationMode="pessimistic"
          mutationOptions={{ meta: { mutation: DELETE_COMPANY } }}
        />
      </Datagrid>
    </List>
  );
};

/**
 * Type of data object received when executing the query
 */
type QueryResultType = ResultOf<typeof COMPANY_LIST>;
/**
 * Type of the items list
 */
type ItemListType = QueryResultType["companyList"];
/**
 * Type of single item
 */
type ItemType = { id: string } & Exclude<Exclude<ItemListType, null | undefined>[0], undefined>;
